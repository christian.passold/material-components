package com.hbsis.android.materialcomponents

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.hbsis.android.components.MaterialTextInputLayout
import com.hbsis.android.components.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val in1: MaterialTextInputLayout = findViewById(R.id.in1)
        val in2: MaterialTextInputLayout = findViewById(R.id.in2)

        in1.error = null
        in1.helperText = "Normal"

        in2.error = "Error message"
        in2.helperText = "Error message"
        in2.isErrorEnabled = true
    }
}
