package com.hbsis.android.components

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText

class MaterialTextInputEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : TextInputEditText(context, attrs, defStyleAttr)