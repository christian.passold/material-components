package com.hbsis.android.components

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.core.content.ContextCompat.getColor
import com.google.android.material.textfield.TextInputLayout

class MaterialTextInputLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : TextInputLayout(context, attrs, defStyleAttr) {

    init {
        boxBackgroundMode = BOX_BACKGROUND_FILLED
        boxBackgroundColor = getColor(getContext(), R.color.colorSurface)
        hintTextColor = ColorStateList.valueOf(getColor(getContext(), R.color.textColorDetail))
        setErrorTextAppearance(R.style.AppThemeComponents_TextInputLayout_TextAppearance_Error)
        setHintTextAppearance(R.style.AppThemeComponents_TextInputLayout_TextAppearance_Hint)
    }

}